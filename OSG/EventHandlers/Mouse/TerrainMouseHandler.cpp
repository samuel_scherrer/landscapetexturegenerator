#include "TerrainMouseHandler.h"

using namespace osg;
using namespace osgViewer;
using namespace osgGA;
using namespace osgUtil;
using namespace osgTCC;

TerrainMouseHandler::TerrainMouseHandler(EventStatus *eventStatus) : GUIEventHandler() {
	// Come�amos com o bot�o esquerdo n�o pressionado
	leftButtonIsPressed = false;
	// Zeramos o marcador de tempo decorrido
	lastTime = 0;

	_eventStatus = eventStatus;
}

bool TerrainMouseHandler::handle(const GUIEventAdapter &ea,
                                  GUIActionAdapter &aa, osg::Object *,
                                  osg::NodeVisitor *nv) {

	// Atualizamos o tempo decorrido
	double elapsedTime = nv->getFrameStamp()->getSimulationTime() - lastTime;
	lastTime = nv->getFrameStamp()->getSimulationTime();
	// Checamos se estamos lidando com o pressionamento do mouse junto da tecla shift ou ctrl
	if (!(ea.getButtonMask()&GUIEventAdapter::LEFT_MOUSE_BUTTON)
		|| !(ea.getModKeyMask()&GUIEventAdapter::MODKEY_CTRL | ea.getModKeyMask()&GUIEventAdapter::MODKEY_SHIFT)
		|| (ea.getModKeyMask()&GUIEventAdapter::MODKEY_CTRL && ea.getModKeyMask()&GUIEventAdapter::MODKEY_SHIFT))
		return false;

	// Checamos se a editorHUD est� ativa
	if (_eventStatus->editorHudStatus != HUD_INUSE){
		return false;
	}

	// Checamos se o bot�o esquerdo foi liberado
	if (ea.getEventType() == GUIEventAdapter::RELEASE) {
		leftButtonIsPressed = false;
		return false;
	}
	leftButtonIsPressed = true;

	// Decidimos se estamos pressionando shift (para baixo) ou ctrl (para cima)
	bool down = true;
	if (ea.getModKeyMask()&GUIEventAdapter::MODKEY_CTRL) 
		down = false;

	// Agora podemos definir a intersec��o com o terreno, se houver
	ref_ptr<LineSegmentIntersector> intersector = 
		new LineSegmentIntersector(Intersector::WINDOW, ea.getX(), ea.getY());

	IntersectionVisitor iv = IntersectionVisitor(intersector.get());

	Viewer* viewer = dynamic_cast<Viewer*>(&aa);

	viewer->getCamera()->accept(iv);

	// Checamos se houve intersec��o
	if (intersector->containsIntersections()) {
		// Obtemos os resultados das intersec��es e procuramos por uma que seja com o terreno
		for (LineSegmentIntersector::Intersections::iterator it = intersector->getIntersections().begin();
			it != intersector->getIntersections().end(); it++) {
				HeightFieldGeode* hf = dynamic_cast<HeightFieldGeode*>(it->nodePath.back());
				if (hf) {
					Vec3 position = it->getWorldIntersectPoint();
					hf->changeHeight(position, down, elapsedTime);
					return true;
				}
		}
	}

	return false;
}