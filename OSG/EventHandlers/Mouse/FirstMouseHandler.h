#ifndef FIRSTMOUSEHANDLER_H
#define FIRSTMOUSEHANDLER_H

#include <osgGA/GUIEventHandler>
#include <osgGA/GUIEventAdapter>
#include <osgGA/GUIActionAdapter>

#include <osgViewer/config/SingleWindow>
#include <osgViewer/Viewer>

#include "../../UserDatas/EventStatus.h"

namespace osgTCC {
	class FirstMouseHandler : public osgGA::GUIEventHandler {
	public:
		FirstMouseHandler(EventStatus *eventStatus);
		virtual bool handle(const osgGA::GUIEventAdapter &ea,
			osgGA::GUIActionAdapter &aa, osg::Object *,
			osg::NodeVisitor *nv);

	protected:
		~FirstMouseHandler() {}

		osg::ref_ptr<EventStatus> _eventStatus;
	};
}

#endif /* FIRSTMOUSEHANDLER_H */