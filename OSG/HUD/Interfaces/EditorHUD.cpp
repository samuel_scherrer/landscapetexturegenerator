#include "EditorHUD.h"

using namespace osgTCC;

EditorHUD::EditorHUD(float windowWidth, float windowHeight, float radius) : 
	CircleHUD(windowWidth, windowHeight, radius, osg::Vec2f(0, 0), 0.6, INT_MAX - 5, false, osg::Vec4f(1,0,0,0.8)) {
		_radiusStep = 2;
		_radiusMax = 100;
		_radiusMin = 2;		
}

void EditorHUD::increaseRadius(){
	if(_radius < _radiusMax)
		_radius += _radiusStep;
}

void EditorHUD::decreaseRadius(){
	if(_radius > _radiusMin)
		_radius -= _radiusStep;
}