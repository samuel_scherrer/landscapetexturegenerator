#ifndef INPUTHUD_H
#define INPUTHUD_H

#include <osgViewer/Viewer>

#include "../Basic/RectangleHUD.h"
#include "../Buttons/Button.h"
#include "../Buttons/InputBox.h"
#include "../Buttons/CheckBox.h"
#include "../Basic/Label.h"

#include <osgText/String>

namespace osgTCC {
	enum checkBoxCombination {
		BOTH,
		PATHONLY,
		SHADERONLY,
		NONE
	};
	class InputHUD : public osgTCC::RectangleHUD {
	public:
		InputHUD(float windowWidth, float windowHeight, const char* currVertexShaderName, const char* currFragShaderName, const char* currHeightmapName);
		Button* getOkButton();
		bool checkCollision(float x, float y);
		std::string getInputPath();
		std::string getFragShaderPath();
		std::string getVertShaderPath();
		int getCheckBoxesCombination();
		virtual void resize(double windowWidth, double windowHeight);
		void update(char letter);
		void update();
		void setIsWriting(bool stop);
		void resetCheckBoxes();
		void removeLabel();
		void setLabel();

	protected:
		// Destrutor protegido.
		virtual ~InputHUD() {}

	private:
		Button* _okButton;
		InputBox* _inputPath;
		InputBox* _inputFragShader;
		InputBox* _inputVertShader;
		CheckBox* _pathCheck;
		CheckBox* _shaderCheck;
		Label* _labelLoad;
		checkBoxCombination _checkBoxComb;



	};
}
#endif /* INPUTHUD_H */