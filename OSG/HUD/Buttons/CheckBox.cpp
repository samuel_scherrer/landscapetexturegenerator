#include "CheckBox.h"

using namespace osgTCC;

CheckBox::CheckBox(float windowWidth, float windowHeight, float edgeLength,	osg::Vec2f pos, float alpha,
				   bool isRelativePos, std::string checkLabel, osg::Vec4 labelColor, osg::Vec4 buttonColor) : 
Button(windowWidth, windowHeight, edgeLength, edgeLength, pos, alpha, isRelativePos, buttonColor){
	initialize(checkLabel, labelColor, edgeLength);
}

CheckBox::CheckBox(float windowWidth, float windowHeight, float edgeLength,	osg::Vec2f pos, float alpha,
				   int renderPriority, bool isRelativePos, std::string checkLabel, osg::Vec4 labelColor, osg::Vec4 buttonColor) :
Button(windowWidth, windowHeight, edgeLength, edgeLength, pos, alpha, renderPriority, isRelativePos, buttonColor){
	initialize(checkLabel, labelColor, edgeLength);
}

void CheckBox::initialize(std::string label, osg::Vec4 labelColor, float edgeLength){
	_fontSize = edgeLength;
	
	_label = new osgText::Text();

	_buttonHUD->getSecondaryGeode()->addDrawable(_label);
	
	_label->setCharacterSize(_fontSize);
    _label->setFont("C:/WINDOWS/Fonts/impact.ttf");
    _label->setText(label);
	_labelText = label;
    _label->setAxisAlignment(osgText::Text::SCREEN);

	if(_windowWidth - _buttonPos.x() > (_fontSize*label.length()/2)){
		_label->setPosition(osg::Vec3f(_buttonPos.x() + _buttonWidth + 1, _buttonPos.y() - _buttonHeight/4, 0));
	}
	else{
		_label->setPosition(osg::Vec3f(_buttonPos.x() - (_buttonWidth + label.length()*_fontSize/2), _buttonPos.y() - _buttonHeight/4, 0));
	}
		
	_label->setColor(osg::Vec4(labelColor.x(),labelColor.y(),labelColor.z(),labelColor.w()));

	//Iniciamos o buttonText que sera usado para marcar X caso o checkbox esteja selecionado
	_buttonText->setCharacterSize(_buttonWidth);
    _buttonText->setFont("C:/WINDOWS/Fonts/impact.ttf");
    _buttonText->setAxisAlignment(osgText::Text::SCREEN);
	_buttonText->setText("");
	_buttonText->setPosition(osg::Vec3f(_buttonPos.x() - _buttonWidth/4, _buttonPos.y() - _buttonHeight/3, 0));
	_buttonText->setColor(osg::Vec4(labelColor.x(),labelColor.y(),labelColor.z(),labelColor.w()));

	//Usado para evitar com quem o clique seja executado muito r�pido
	_delay = 5;
}

void CheckBox::setButtonText(){
	if(_delay == 5){
		if(_buttonText->getText().createUTF8EncodedString() ==  "")
			_buttonText->setText("X");
		else
			_buttonText->setText("");
		_delay = 0;
	}
	_delay++;
}

bool CheckBox::getCheckBoxMark(){
	if(_buttonText->getText().createUTF8EncodedString() == "")
		return false;
	else
		return true;
}

void CheckBox::clearMark(){
	if(_buttonText->getText().createUTF8EncodedString() == "X")
		_buttonText->setText("");
}

void CheckBox::reDraw(double windowWidth, double windowHeight, float HUDwidth, float HUDheight,
						osg::Vec2f centerPos, bool isRelativePos) {

	Button::reDraw(windowWidth, windowHeight, HUDwidth, HUDheight,
			centerPos, isRelativePos);

	if(_windowWidth - _buttonPos.x() > (_fontSize*_labelText.length()/2)){
		_label->setPosition(osg::Vec3f(_buttonPos.x() + _buttonWidth + 1, _buttonPos.y() - _buttonHeight/4, 0));
	}
	else{
		_label->setPosition(osg::Vec3f(_buttonPos.x() - (_buttonWidth + _labelText.length()*_fontSize/2), _buttonPos.y() - _buttonHeight/4, 0));
	}

	//Como � um checkBox temos que reposicionar o texto do button pois esse � posicionado de forma diferente em rela��o ao seu
	//pai button
	_buttonText->setPosition(osg::Vec3f(_buttonPos.x() - _buttonWidth/4, _buttonPos.y() - _buttonHeight/3, 0));
}