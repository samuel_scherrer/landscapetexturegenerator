#ifndef BUTTON_H
#define BUTTON_H

#include <osgText/Text>
#include <osgText/Font>
#include <osgText/String>

#include "../../HUD/Basic/RectangleHUD.h"

namespace osgTCC {
	enum buttonTextPos {
		LEFT,
		CENTER,
		RIGHT
	};
	class Button {
	public:
		Button(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight,
					osg::Vec2f pos, float alpha, bool isRelativePos, osg::Vec4 buttonColor);
		Button(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight,
					osg::Vec2f pos, float alpha, int renderPriority, bool isRelativePos, osg::Vec4 buttonColor);
		RectangleHUD* getButtonHud();
		void setButtonText(int fontSize, std::string buttonText, buttonTextPos textPos, osg::Vec4f textColor);
		void setButtonText(std::string text);
		std::string getButtonText();
		bool getCollision(float x, float y);
		void reDraw(double windowWidth, double windowHeight, float HUDwidth, float HUDheight,
						osg::Vec2f centerPos, bool isRelativePos);

	protected:
		// Destrutor protegido.
		virtual ~Button() {}

		void confAlphaStateSet(float alpha);
		void confPriorityStateSet(int renderPriority);
		void Button::initialize(float windowWidth, float windowHeight, float buttonWidth, float buttonHeight, osg::Vec2f pos,
							float alpha, bool isRelativePos);

		osg::ref_ptr<RectangleHUD> _buttonHUD;
		osg::ref_ptr<osgText::Text> _buttonText;
		osg::ref_ptr<osg::Geode> textGeode;
		float _buttonWidth;
		float _buttonHeight;
		float _windowWidth;
		float _windowHeight;
		osg::Vec2f _buttonPos;
		osg::Geode* textGeo;
		buttonTextPos _textPos;
	};
}
#endif /* BUTTON_H */