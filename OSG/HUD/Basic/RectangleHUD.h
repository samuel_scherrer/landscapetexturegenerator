#ifndef RECTANGLEHUD_H
#define RECTANGLEHUD_H

#include "HudProjection.h"

namespace osgTCC {
	class RectangleHUD : public HudProjection {
	public:
		RectangleHUD(double windowWidth, double windowHeight, float HUDwidth, float HUDheight, osg::Vec2f centerPos,
						float alpha, int renderPriority, bool isRelativePos, osg::Vec4 HUDcolor = osg::Vec4(0,0,1,1));
		void reDraw(double windowWidth, double windowHeight, float HUDwidth, float HUDheight,
						osg::Vec2f centerPos, bool isRelativePos);
		float getHUDwidth() { return _HUDwidth; }
		float getHUDheight() { return _HUDheight; }

	protected:
		virtual void initSquareBackground(osg::Vec2f centerPos, float edgeLength, bool isRelativePos);
		virtual void initCircleBackground(float radius, osg::Vec2f centerPos, bool isRelativePos);
		virtual void initRectangleBackground(float widthLength, float heightLength, osg::Vec2f centerPos, bool isRelativePos);
		float _HUDwidth;
		float _HUDheight;
	};
}
#endif /* RECTANGLEHUD_H */