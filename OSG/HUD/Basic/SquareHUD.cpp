#include "SquareHUD.h"

using namespace osgTCC;
using namespace osg;

SquareHUD::SquareHUD(double windowWidth, double windowHeight, osg::Vec2f centerPos,
					 float edgeLength, float alpha, int renderPriority, bool isRelativePos, osg::Vec4 HUDcolor)
	: HudProjection(windowWidth, windowHeight, alpha, renderPriority) {
	_HUDcolor->push_back(HUDcolor);
	initSquareBackground(edgeLength, centerPos, isRelativePos);
}

void SquareHUD::initSquareBackground(float edgeLength, osg::Vec2f centerPos, bool isRelativePos) {
	float halfEdge = edgeLength/2;
	_HUDBackgroundVertices = new Vec3Array();

	if(!isRelativePos){
		_HUDBackgroundVertices->push_back(Vec3(centerPos.x() - halfEdge, centerPos.y() - halfEdge, -1));
		_HUDBackgroundVertices->push_back(Vec3(centerPos.x() - halfEdge, centerPos.y() + halfEdge, -1));
		_HUDBackgroundVertices->push_back(Vec3(centerPos.x() + halfEdge, centerPos.y() + halfEdge, -1));
		_HUDBackgroundVertices->push_back(Vec3(centerPos.x() + halfEdge, centerPos.y() - halfEdge, -1));
	}
	else{
		float posX = _windowWidth*(centerPos.x()/100);
		float posY = _windowHeight*(centerPos.y()/100);

		_HUDBackgroundVertices->push_back(Vec3(posX - halfEdge, posY - halfEdge, -1));
		_HUDBackgroundVertices->push_back(Vec3(posX - halfEdge, posY + halfEdge, -1));
		_HUDBackgroundVertices->push_back(Vec3(posX + halfEdge, posY + halfEdge, -1));
		_HUDBackgroundVertices->push_back(Vec3(posX + halfEdge, posY - halfEdge, -1));
	}
	
	_HUDBackgroundIndices = new DrawElementsUInt(osg::PrimitiveSet::POLYGON, 0);
	_HUDBackgroundIndices->push_back(0);
	_HUDBackgroundIndices->push_back(1);
	_HUDBackgroundIndices->push_back(2);
	_HUDBackgroundIndices->push_back(3);

	_HUDBackground->setVertexArray(_HUDBackgroundVertices);
	_HUDBackground->addPrimitiveSet(_HUDBackgroundIndices);
	_HUDBackground->setColorArray(_HUDcolor);
	_HUDBackground->setColorBinding(Geometry::BIND_OVERALL);

	_edgeLength = edgeLength;
}

void SquareHUD::reDraw(double windowWidth, double windowHeight, float edgeLenght, osg::Vec2f centerPos, bool isRelativePos){
	//Primeiramente removemos o _HUDbackground dos drawbles do geode da HUD
	_HUDGeode->removeDrawable(_HUDBackground);

	//Criamos um novo geometry
	_HUDBackground = new osg::Geometry();
	//Inicializamos o geometry criado
	initCircleBackground(edgeLenght, centerPos, isRelativePos);
	
	//Adicionamos novamente o _HUDbackground aos drawbles do geode da HUD
	_HUDGeode->addDrawable(_HUDBackground);

	//Informamos o resize para a matrix responsável pela ortogonalidade 
	setMatrix(osg::Matrix::ortho2D(0, windowWidth, 0, windowHeight));

	this->_windowWidth = windowWidth;
	this->_windowHeight = windowHeight;
}

void SquareHUD::initCircleBackground(float radius, osg::Vec2f centerPos, bool isRelativePos){}
void SquareHUD::initRectangleBackground(float widthLength, float heightLength, osg::Vec2f centerPos, bool isRelativePos){}

