#ifndef SQUAREHUD_H
#define SQUAREHUD_H

#include "HudProjection.h"

namespace osgTCC {
	class SquareHUD : public HudProjection {
	public:
		SquareHUD(double windowWidth, double windowHeight, osg::Vec2f centerPos, float edgeLength,
			float alpha, int renderPriority, bool isRelativePos, osg::Vec4 HUDcolor = osg::Vec4(0,0,1,1));
		void reDraw(double windowWidth, double windowHeight, float edgeLenght,
						osg::Vec2f centerPos, bool isRelativePos);
		float getEdgeLength() { return _edgeLength; }

	protected:
		virtual void initSquareBackground(float edgeLength, osg::Vec2f centerPos, bool isRelativePos);
		virtual void initCircleBackground(float radius, osg::Vec2f centerPos, bool isRelativePos);
		virtual void initRectangleBackground(float widthLength, float heightLength, osg::Vec2f centerPos, bool isRelativePos);
		float _edgeLength;
	};
}
#endif /* SQUAREHUD_H */