#ifndef __SQUAREGEODE_H__
#define __SQUAREGEODE_H__

#include <osg/Geode>

namespace osgTCC {
	class SquareGeode : public osg::Geode {
	public:
		SquareGeode(float edgeLength, const osg::Vec3 &position = osg::Vec3(0, 0, 0));

	protected:
		virtual ~SquareGeode() {}
	};
}
#endif // !__SQUAREGEODE_H__