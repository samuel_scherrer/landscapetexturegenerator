#include "HeightFieldVertex.h"

#include "HeightFieldTriangle.h"

#include "../../Utils/Utils.h"

using namespace osgTCC;
using namespace osg;

HeightFieldVertex::HeightFieldVertex(Vec3& position) {
	// Associamos a posi��o
	_position = position;
	// Marcamos o v�rtice como modificado
	_dirty = true;
}

void HeightFieldVertex::addTriangle(HeightFieldTriangle* triangle) {
	// Checamos se o tri�ngulo n�o � nulo
	if (triangle == nullptr)
		showErrorMessage("O tri�ngulo passado para o HeightfieldVertex n�o pode ser nulo.", 9);
	// Adicionamos o tri�ngulo
	_triangles.insert(triangle);
}

void HeightFieldVertex::updateNormal() {
	// Devemos iterar sob a lista de tri�ngulos
	std::set<osgTCC::HeightFieldTriangle*>::iterator it;
	for (it = _triangles.begin(); it != _triangles.end(); it++) {
		if ((*it)->isDirty()) {
			// Computamos a normal e abortamos a procura
			computeNormal();
			break;
		}
	}
	// Desmarcamos o v�rtice como modificado
	_dirty = false;
}

void HeightFieldVertex::computeNormal() {
	// Devemos iterar sob a lista de tri�ngulos
	std::set<HeightFieldTriangle*>::iterator it;
	// Zeramos a normal armazenada
	_normal = Vec3(0, 0, 0);
	for (it = _triangles.begin(); it != _triangles.end(); it++) {
		_normal += (*it)->getNormal();
	}
	// Normalizamos
	_normal.normalize();
}

void HeightFieldVertex::changeHeight(float deltaHeight) {
	_position.z() += deltaHeight;
	_dirty = true;
}