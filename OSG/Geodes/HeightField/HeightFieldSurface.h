#ifndef __HEIGHTFIELDSURFACE_H__
#define __HEIGHTFIELDSURFACE_H__

#include <osg/Vec3>
#include <osg/Array>
#include <set>
#include <vector>

namespace osgTCC {
	class HeightFieldTriangle;
	class HeightFieldVertex;

	class HeightFieldSurface {
	protected:
		// Armazena a lista de v�rtices do Heightfield.
		std::vector<HeightFieldVertex*> _vertices;
		// Armazena o conjunto de tri�ngulos que formam a superf�cie.
		std::vector<HeightFieldTriangle*> _triangles;
		// Armazena o vetor de posi��es.
		osg::ref_ptr<osg::Vec3Array> _positions;
		// Armazena o vetor de normais.
		osg::ref_ptr<osg::Vec3Array> _normals;
		// Armazena o raio de mudan�a.
		float _radius;
		// Armazena o fator de escala.
		float _scale;
		// Armazena o n�mero de linhas do Heightmap.
		unsigned int _numberOfRows;
		// Armazena o n�mero de colunas do Heightmap.
		unsigned int _numberOfColumns;
		// Armazena a dist�ncia entre duas linhas do Heightmap
		float _xDistance;
		// Armazena a dist�ncia entre duas colunas do Heightmap
		float _yDistance;

	public:
		// Cria um novo HeightfieldSurface vazio.
		HeightFieldSurface(float radius, float scale, unsigned int numberOfRows, unsigned int numberOfColumns,
			float xDistance, float yDistance);

		// Remove todos os vetores criados
		virtual ~HeightFieldSurface();

		// Adiciona um novo v�rtice � superf�cie.
		void addVertex(osg::Vec3& position);

		// Adiciona um novo tri�ngulo � superf�cie.
		void addTriangle(unsigned int index0, unsigned int index1, unsigned int index2);

		// M�todo respons�vel por modificar a altura em torno da posi��o informada.
		void changeHeight(osg::Vec3 centerPosition, bool down, double elapsedTime);
		// Retorna o vetor de posi��es.
		osg::ref_ptr<osg::Vec3Array> getPositions();
		// Atualiza todas as normais.
		void updateAllNormals();

		// Retorna o vetor de normais.
		osg::ref_ptr<osg::Vec3Array> getNormals();

		// Retorna o primeriro v�rtice.
		osg::Vec3 getFirtVertex();

		// Retorna o raio de modifica��o atual.
		float getRadius();
		// Define o novo valor do raio de modifica��o.
		void setRadius(float radius);
		// Retorna o fator de escala atual.
		float getScale();
		// Define o novo valor do fator de escala.
		void setScale(float scale);

	protected:
		// Retorna a altura mais alta da superf�cie (maior z).
		float getHighestHeight();
		// Retorna a altura mais baixa da superf�cie (menor z).
		float getLowestHeight();

	};
}
#endif // !__HEIGHTFIELDSURFACE_H__