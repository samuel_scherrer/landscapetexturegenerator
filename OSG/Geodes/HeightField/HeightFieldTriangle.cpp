#include "HeightFieldTriangle.h"

#include "HeightFieldVertex.h"

#include "../../Utils/Utils.h"

using namespace osgTCC;
using namespace osg;

HeightFieldTriangle::HeightFieldTriangle(HeightFieldVertex* vertex0, HeightFieldVertex* vertex1, 
										 HeightFieldVertex* vertex2) {
	// Checamos se os v�rtices n�o s�o nulos
	if (vertex0 == nullptr || vertex1 == nullptr || vertex2 == nullptr)
		showErrorMessage("Os v�rtices passados para o HightfieldTriangle n�o podem ser nulos.", 9);
	// Associamos os v�rtices
	_vertex0 = vertex0;
	_vertex1 = vertex1;
	_vertex2 = vertex2;
	// Marcamos o tri�ngulo como modificado
	_dirty = true;
	// Adicionamos o tri�ngulo ao v�rtice
	_vertex0->addTriangle(this);
	_vertex1->addTriangle(this);
	_vertex2->addTriangle(this);
}

void HeightFieldTriangle::updateNormal() {
	// Precisamos checar se um dos v�rtices foi modificado
	if (_vertex0->isDirty() || _vertex1->isDirty() || _vertex2->isDirty())
		computeNormal();
}

void HeightFieldTriangle::computeNormal() {
	// Devemos realizar o produto vetorial de (vertex2 - vertex1) e (vertex0 - vertex1)
	_normal = (_vertex2->getPosition() - _vertex1->getPosition()) ^ (_vertex0->getPosition() - _vertex1->getPosition());
	_dirty = true;
}