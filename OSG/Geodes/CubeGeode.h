#ifndef __CUBEGEODE_H__
#define __CUBEGEODE_H__

#include <osg/Geode>

namespace osgTCC {
	class CubeGeode : public osg::Geode {
	public:
		CubeGeode(float edgeLength, const osg::Vec3 &position = osg::Vec3(0, 0, 0));

	protected:
		virtual ~CubeGeode() { }

	};
}
#endif // !__CUBEGEODE_H__