#ifndef __UTILS_H__
#define __UTILS_H__

#include <osg/Image>
#include <osg/Node>
#include <osg/Shader>

namespace osgTCC {
	// Informa a mensagem na sa�da padr�o e termina o programa, informando o n�mero
	// do erro.
	void showErrorMessage(const std::string &message, int error);

	// Abre o arquivo informado e retorna um Image.
	osg::ref_ptr<osg::Image> getImage(const std::string &fileName);

	// Abre o arquivo informado e retorna um Shader do tipo declarado.
	osg::ref_ptr<osg::Shader> getShader(osg::Shader::Type type, const std::string &fileName);

	// Define o modo wireframe como ligado ou desligado para o Node informado.
	void wireFrameMode(osg::Node *node, bool on);

	// Define o modo de textura como ligado ou desligado para o Node informado.
	void textureMode(osg::Node *node, bool on);

	// Define o modo de ilumina��o como ligado ou desligado para o Node informado.
	void lightingMode(osg::Node *node, bool on);

	// Encontra um n� filho do n� passado
	osg::Node* findNamedNode(const std::string& seachName, osg::Node* currNode);
}
#endif // !__UTILS_H__