#include "Viewers/WindowedViewer.h"

#include "UserDatas/EventStatus.h"

#include "Utils/Utils.h"
#include "Utils/Factory.h"

#include <iostream>

using namespace osgTCC;

int main() {
	// Criamos um StatusData e a Factory
	osg::ref_ptr<EventStatus> eventStatus = new EventStatus(800, 600);

	// Criamos a janela principal
	WindowedViewer v = WindowedViewer("TCC", eventStatus->windowWidth, eventStatus->windowHeight, 300, 100);
	v.realize();

	// Criamos o factory que ira realizar as demais instanciacoes
	Factory* factory = new Factory(eventStatus);

	// Adicionamos os handlers que serao usados
	factory->addEventHandlers(&v);

	// Criamos o grafo e pegamos o root
	osg::Group* root = factory->initializeGraph();

	// Criamos a camera que usuaremos
	factory->setupCamera(&v, osg::Vec3f(0,-100,60), osg::Vec3f(0,10,0), eventStatus->windowWidth, eventStatus->windowHeight);

	// Definimos os shaders bem como suas variáveis
	factory->setupShaders();

	// Associamos a raiz da cena e rodamos
	v.setSceneData(root);

	//Loop principal
	v.run();
}