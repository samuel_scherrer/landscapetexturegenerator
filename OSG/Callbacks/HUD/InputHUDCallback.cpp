#include "InputHUDCallback.h"

using namespace osgTCC;

InputHUDCallback::InputHUDCallback(EventStatus *eventStatus) {
	_eventStatus = eventStatus;
}

void InputHUDCallback::operator()(osg::Node *node, osg::NodeVisitor *nv) {
	InputHUD *inputHUD = dynamic_cast<InputHUD*>(node);
	
	if(_eventStatus->resizeEvent){
		inputHUD->resize(_eventStatus->windowWidth, _eventStatus->windowHeight);
	}

	//Abre ou fecha a HUD de input
	if(_eventStatus->inputHudStatus == HUD_OPEN){
		_eventStatus->cameraEnabled = false;
		inputHUD->setNodeMask(0xFFFFFFFF);
		_eventStatus->keyRead = true;
		_eventStatus->inputHudStatus = HUD_INUSE;
		_eventStatus->editorHudStatus = HUD_CLOSE;
		_eventStatus->freeMouseToggle = MFM_ON;
	}
	else if(_eventStatus->inputHudStatus == HUD_CLOSE){
		_eventStatus->cameraEnabled = true;
		inputHUD->setNodeMask(0xFFFFFFFE);
		inputHUD->setIsWriting(false);
		inputHUD->update();
		inputHUD->removeLabel();
		_eventStatus->inputHudStatus = HUD_NOP;
		_eventStatus->freeMouseToggle = MFM_TRANSITION;
	}

	//Caso a HUD esteja ativa...
	if(_eventStatus->inputHudStatus == HUD_INUSE){
		if(_eventStatus->mouseLeftB){
			if(inputHUD->checkCollision(_eventStatus->mouseX, _eventStatus->mouseY)){
				// Se clicou no oks
				switch(inputHUD->getCheckBoxesCombination()){
					case BOTH:
						_eventStatus->heightmapName = inputHUD->getInputPath();
						_eventStatus->fragShaderName = inputHUD->getFragShaderPath();
						_eventStatus->vertShaderName = inputHUD->getVertShaderPath();
						_eventStatus->heightmapChanges = HM_BOTH;;
						// Sinaliza o loading
						inputHUD->setLabel();
					break;
					case PATHONLY:
						_eventStatus->heightmapName = inputHUD->getInputPath();
						_eventStatus->heightmapChanges = HM_TEXTURE;
						// Sinaliza o loading
						inputHUD->setLabel();
					break;
					case SHADERONLY:
						_eventStatus->fragShaderName = inputHUD->getFragShaderPath();
						_eventStatus->vertShaderName = inputHUD->getVertShaderPath();
						_eventStatus->heightmapChanges = HM_SHADER;
						// Sinaliza o loading
						inputHUD->setLabel();	
					break;
				}
				_eventStatus->inputHudStatus = HUD_CLOSE;
				inputHUD->resetCheckBoxes();
			}
		}
		// Se nao clicou no mouse verifica se h� uma tecla digitada
		else{
			if(!_eventStatus->keyRead){
				inputHUD->update((char)_eventStatus->keyboardKeyValue);
				_eventStatus->keyRead = true;
			}
			inputHUD->update();
		}
	}
	traverse(node, nv);
}