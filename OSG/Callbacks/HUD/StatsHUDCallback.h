#ifndef STATSHUDCALLBACK_H
#define STATSHUDCALLBACK_H

#include <osg/NodeCallback>

#include "../../UserDatas/EventStatus.h"

namespace osgTCC {
	class StatsHUDCallback : public osg::NodeCallback {
	public:
		StatsHUDCallback(EventStatus *eventStatus);
		virtual void StatsHUDCallback::operator()(osg::Node *node, osg::NodeVisitor *nv);

	protected:
		// Destrutor protegido
		virtual ~StatsHUDCallback() {}

		osg::ref_ptr<EventStatus> _eventStatus;
	};
}
#endif /* STATSHUDCALLBACK_H */