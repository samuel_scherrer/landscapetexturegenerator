#include "editorHUDCallback.h"

using namespace osgTCC;

EditorHUDCallback::EditorHUDCallback(EventStatus *eventStatus){
	_eventStatus = eventStatus;
}

void EditorHUDCallback::operator()(osg::Node *node, osg::NodeVisitor *n){
	EditorHUD *editorHUD = dynamic_cast<EditorHUD*>(node);

	if(_eventStatus->resizeEvent){
		editorHUD->resize(_eventStatus->windowWidth, _eventStatus->windowHeight);
	}
	
	if(_eventStatus->editorHudStatus == HUD_OPEN){
		editorHUD->setNodeMask(0xFFFFFFFF);
		_eventStatus->inputHudStatus = HUD_CLOSE;
	}
	else if(_eventStatus->editorHudStatus == HUD_CLOSE){
		editorHUD->setNodeMask(0xFFFFFFFE);
	}

	// Se a HUD esta em uso...
	if(_eventStatus->editorHudStatus == HUD_INUSE){
		if(_eventStatus->AOEinfo == INCREASE_AOE){
			editorHUD->increaseRadius();
		}
		else if(_eventStatus->AOEinfo == DECREASE_AOE){
			editorHUD->decreaseRadius();
		}

		if(_eventStatus->AOEinfo != NOP)
		{
			_eventStatus->aoe = editorHUD->getRadius();

			editorHUD->reDraw(_eventStatus->windowWidth, _eventStatus->windowHeight, _eventStatus->aoe,
								osg::Vec2f(0, 0), false);

			_eventStatus->AOEinfo = NOP;
		}
		editorHUD->setPositionTransf(osg::Vec3f(_eventStatus->mouseX, _eventStatus->mouseY, 0));
	}

	traverse(node, n);
}