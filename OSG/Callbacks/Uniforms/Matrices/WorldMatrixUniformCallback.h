#ifndef __WORLDMATRIXUNIFORMCALLBACK_H__
#define __WORLDMATRIXUNIFORMCALLBACK_H__

#include <osg/Uniform>

namespace osgTCC {
	class WorldMatrixUniformCallback : public osg::Uniform::Callback {
	public:
		virtual void operator()(osg::Uniform *uniform, osg::NodeVisitor *nv);

	protected:
		// Destrutor protegido
		virtual ~WorldMatrixUniformCallback() {}
	};
}
#endif // !__WORLDUNIFORMCALLBACK_H__