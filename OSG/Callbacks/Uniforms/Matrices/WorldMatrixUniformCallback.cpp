#include "WorldMatrixUniformCallback.h"

#include <osg/NodeVisitor>

using namespace osg;
using namespace osgTCC;

void WorldMatrixUniformCallback::operator()(osg::Uniform* uniform, osg::NodeVisitor* nv) {
	// Atualizamos o valor da uniform
	uniform->set(*nv->getNodePath().back()->getWorldMatrices().begin());
}