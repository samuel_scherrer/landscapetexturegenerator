#include "ViewMatrixUniformCallback.h"

using namespace osg;
using namespace osgTCC;

ViewMatrixUniformCallback::ViewMatrixUniformCallback(Camera* camera) : Uniform::Callback() {
	// Associamos a c�mera
	this->camera = camera;
}

void ViewMatrixUniformCallback::operator()(osg::Uniform* uniform, osg::NodeVisitor* nv) {
	// Atualizamos o valor da uniform
	uniform->set(camera->getViewMatrix());
}