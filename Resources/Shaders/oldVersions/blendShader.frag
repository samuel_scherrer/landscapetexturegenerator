#version 410

in vec2 texCoord;
in vec3 currPos;
in vec3 terrainNormal;

out vec4 fragColor;

uniform sampler2D sandSampler;
uniform sampler2D grassSampler;
uniform sampler2D rockSampler;
uniform sampler2D snowSampler;

uniform vec3 lightColor;
uniform float lightAmbIntensity;
uniform vec3 lightDirection;
uniform float lightDiffuseIntensity;

//Metodos para calcular a luz
vec4 getDiffuseColor(vec3 normal, vec3 lightDir, float difIntensity, vec3 lightColor);
vec4 getAmbientColor(vec3 lightColor, float ambIntensity);

void main() {
	if(currPos.z < 3.0f){
		fragColor = texture(sandSampler, texCoord.st)* (getAmbientColor(lightColor, lightAmbIntensity) +
						 getDiffuseColor(terrainNormal, lightDirection, lightDiffuseIntensity, lightColor));
	}
	else if(currPos.z > 3.0f && currPos.z < 5.0f){
		fragColor =  ((1.0f - currPos.z/5.0f) * texture(sandSampler, texCoord.st) + currPos.z/5.0f * texture(grassSampler, texCoord.st)) * (getAmbientColor(lightColor, lightAmbIntensity) +
						 getDiffuseColor(terrainNormal, lightDirection, lightDiffuseIntensity, lightColor));
	}
	else if(currPos.z < 8.0f){
	    fragColor = texture(grassSampler, texCoord.st) * (getAmbientColor(lightColor, lightAmbIntensity) +
						 getDiffuseColor(terrainNormal, lightDirection, lightDiffuseIntensity, lightColor));
	}
	else if(currPos.z > 8.0f && currPos.z < 10.0f){
		fragColor =	(texture(grassSampler, texCoord.st) + texture(rockSampler, texCoord.st)) * (getAmbientColor(lightColor, lightAmbIntensity) +
						 getDiffuseColor(terrainNormal, lightDirection, lightDiffuseIntensity, lightColor));
	}
	else if(currPos.z < 25.0f){
		fragColor = texture(rockSampler, texCoord.st) * (getAmbientColor(lightColor, lightAmbIntensity) +
						 getDiffuseColor(terrainNormal, lightDirection, lightDiffuseIntensity, lightColor));
	}
	else if(currPos.z > 25.0f && currPos.z < 28.0f){
		fragColor =  (texture(rockSampler, texCoord.st) + texture(snowSampler, texCoord.st)) * (getAmbientColor(lightColor, lightAmbIntensity) +
						 getDiffuseColor(terrainNormal, lightDirection, lightDiffuseIntensity, lightColor));
	}
	else{
		fragColor = texture(snowSampler, texCoord.st) * (getAmbientColor(lightColor, lightAmbIntensity) +
						 getDiffuseColor(terrainNormal, lightDirection, lightDiffuseIntensity, lightColor));
	}
}

vec4 getDiffuseColor(vec3 normal, vec3 lightDir, float difIntensity, vec3 lightColor){
	float diffuseFactor = dot(normalize(normal), -lightDir);

	vec4 diffuseColor;

	if (diffuseFactor > 0) {
		diffuseColor = vec4(lightColor, 1.0f) * difIntensity * diffuseFactor;
	}
	else {
		diffuseColor = vec4(0, 0, 0, 0);
	}
	return diffuseColor;
}

vec4 getAmbientColor(vec3 lightColor, float ambIntensity){
	return vec4(lightColor, 1.0f) * ambIntensity;
}